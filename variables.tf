variable "tags" {
  type        = map(string)
  default     = {}
  description = "A mapping of tags which should be assigned to all module resources"
}


variable "enable" {
  default     = false
  description = "Destroy all module resources if false"
}


variable "description" {
  type    = string
  default = null
  description = "The description of the key as viewed in AWS console"
}

variable "key_usage" {
  type    = string
  default = null

  validation {
    condition = (var.key_usage != null
      ? contains(["ENCRYPT_DECRYPT", "SIGN_VERIFY"], var.key_usage) : true
    )

    error_message = "Invalid value of \"key_usage\"."
  }

  description = "Specifies the intended use of the key. Valid values: ENCRYPT_DECRYPT or SIGN_VERIFY. Defaults to ENCRYPT_DECRYPT"
}

variable "customer_master_key_spec" {
  type    = string
  default = null

  validation {
    condition = (var.customer_master_key_spec != null ? contains([
      "SYMMETRIC_DEFAULT", "RSA_2048", "RSA_3072", "RSA_4096",
      "ECC_NIST_P256", "ECC_NIST_P384", "ECC_NIST_P521", "ECC_SECG_P256K1"
    ], var.customer_master_key_spec) : true)

    error_message = "Invalid value of \"customer_master_key_spec\"."
  }

  description = "Specifies whether the key contains a symmetric key or an asymmetric key pair and the encryption algorithms or signing algorithms that the key supports. Valid values: SYMMETRIC_DEFAULT, RSA_2048, RSA_3072, RSA_4096, ECC_NIST_P256, ECC_NIST_P384, ECC_NIST_P521, or ECC_SECG_P256K1. Defaults to SYMMETRIC_DEFAULT"
}

variable "bypass_policy_lockout_safety_check" {
  type    = bool
  default = null

  description = "A flag to indicate whether to bypass the key policy lockout safety check. Setting this value to true increases the risk that the KMS key becomes unmanageable. Do not set this value to true indiscriminately. The default value is false"
}

variable "deletion_window_in_days" {
  type    = number
  default = null

  validation {
    condition = (var.deletion_window_in_days != null ?
      7 <= var.deletion_window_in_days && var.deletion_window_in_days <= 30
    : true)

    error_message = "Value of \"deletion_window_in_days\" must be between 7 and 30 inclusive."
  }

  description = "The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between 7 and 30, inclusive. If you do not specify a value, it defaults to 30. If the KMS key is a multi-Region primary key with replicas, the waiting period begins when the last of its replica keys is deleted. Otherwise, the waiting period begins immediately"
}

variable "enable_key_rotation" {
  type    = bool
  default = null

  description = "Specifies whether key rotation is enabled. Defaults to false"
}

variable "multi_region" {
  type    = bool
  default = null

  description = "Indicates whether the KMS key is a multi-Region (true) or regional (false) key. Defaults to false"
}

variable "policy" {
  type    = string
  default = null

  description = "A valid policy JSON document. Although this is a key policy, not an IAM policy, an aws_iam_policy_document, in the form that designates a principal, can be used"
}

variable "name_prefix" {
  default = ""
  description = "The name prefix for the key family"
}

variable "replica_word" {
  default = "replica"

  description = "The word to distinguish replica keys. Defaults to \"replica\""
}

variable "replica_policy" {
  type    = string
  default = null

  description = "A valid policy JSON like var.policy document"
}
