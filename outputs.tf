#################
output "enable" {
  ###############
  value = var.enable
  description = "Path-through value of var.enable"
}

##################
output "kms_key" {
  ################
  value = try(aws_kms_key.this[0], null)
  description = "aws_kms_key object"
}

####################
output "kms_alias" {
  ##################
  value = try(aws_kms_alias.main[0], null)
  description = "aws_kms_alias object"
}

##########################
output "kms_replica_key" {
  ########################
  value = try(aws_kms_replica_key.this[0], null)
  description = "aws_kms_replica_key object"
}

############################
output "kms_replica_alias" {
  ##########################
  value = try(aws_kms_alias.replica[0], null)
  description = "aws_kms_alias for aws_kms_replica_key object"
}
